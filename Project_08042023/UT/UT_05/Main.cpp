#include <iostream>
#include <vector>
#include <list>
#include <bits/stdc++.h>
#include "logic.cpp"
using namespace std;

/*
To check the reorder function is working properly with list , with 14 elements, x_threshold = 12, n = 7

expected result
-----------------
Subset_C should have five elements in ascending order as follows :-
x = 2, y = 1.1
x = 1, y = 1.2
x = 12, y = 1.2
x = 8, y = 1.3
x = 5, y = 2.1
x = 3, y = 2.2
x = 2, y = 3.3
*/ 

void callback_function(Data& data)
{
	cout << "x = " << static_cast<int>(data.x) << ", y = " << data.y <<endl;
}

int main() 
{
	int n = 7;    
	uint8_t x_threshold = 12;	
	vector<Data> vect = {{5, 2.1}, {8,1.3}, {3,2.2}, {2,3.3}, {7,4.4}, 
						 {4, 5.5}, {6,6.6}, {1,1.2}, {2,1.1}, {11,10.3}, 
						 {12,1.2}, {13,14.3}, {14,13.3}, {15,12.1}};
    
	deque<Data> deq(vect.begin(), vect.end()); 
    list<Data> lst(vect.begin(), vect.end()); 
	
	reorder(lst, x_threshold, n, callback_function);	
	
	cout<<"After reorder data\n";
    for (auto data : lst)
	{
		cout << "x = " << static_cast<int>(data.x) << ", y = " << data.y <<endl;
	}	
    return 0;
}