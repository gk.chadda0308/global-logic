#include <iostream>
#include <vector>
#include <list>
#include <bits/stdc++.h>
#include "logic.cpp"
using namespace std;

void callback_function(Data& data)
{
	cout << "x = " << static_cast<int>(data.x) << ", y = " << data.y <<endl;
}

int main() 
{
	int n = 5;    
	uint8_t x_threshold = 8;	
	vector<Data> vect = {{5, 2.1}, {8,1.3}, {3,2.2}, {2,3.3}, {7,4.4}, 
						 {4, 5.5}, {6,6.6}, {1,1.2}, {2,1.1}, {11,10.3}, 
						 {12,1.2}, {13,14.3}, {14,13.3}, {15,12.1}};
    
	deque<Data> deq(vect.begin(), vect.end()); 
    list<Data> lst(vect.begin(), vect.end()); 
	
	reorder(lst, x_threshold, n, callback_function);	
	
	cout<<"After reorder data\n";
    for (auto data : lst)
	{
		cout << "x = " << static_cast<int>(data.x) << ", y = " << data.y <<endl;
	}	
    return 0;
}