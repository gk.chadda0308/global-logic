#include "logic.h"

bool compare_y(const Data& a, const Data& b) 
{
    return a.y < b.y;
}

template<typename Container>
void reorder(Container& cont, uint8_t x_threshold,int n, void (*callback)(Data&)) 
{
	vector<Data> vect(cont.begin(), cont.end()), subset_A, subset_B, subset_C;
	
	for (auto data : vect) 
	{
        if (data.x > x_threshold) 
		{
            subset_A.push_back(data);
        } else 
		{
            subset_B.push_back(data);
        }
    }
    
    if (subset_B.size() > n) 
	{ 
		subset_C = subset_B;
		sort(subset_C.begin(), subset_C.end(), compare_y);
		subset_C.erase(subset_C.begin()+n, subset_C.end());
    } 
	else 
	{
        subset_C = subset_B;
		sort(subset_C.begin(), subset_C.end(), compare_y);
    }
	
	cout<<"callback function start for subset_C\n";
	for_each(subset_C.begin(), subset_C.end(), callback);
	cout<<"callback function end for subset_C\n\n";
	
	subset_A.insert(subset_A.end(), subset_B.begin(), subset_B.end());	
	cont.assign(subset_A.begin(), subset_A.end());		
}