#include <iostream>
#include <vector>
#include <bits/stdc++.h>
#pragma once
using namespace std;

struct Data {
    uint8_t x;
    double y;
};

template<typename Container>
void reorder(Container& cont, uint8_t x_threshold, int n,void (*callback) (Data&));
